---
layout: post
title:  "Hello-world-placeholder post"
date:   2017-02-05 12:00:00 +0000
excerpt_separator: <!--more-->
tags: [misc, rant]
---

"Hello World" post to keep this spot busy. But while we wait...

<!--more-->

## WE GOT HEADERS

## *ALSO IN ITALICS*

### And some smaller ones
#### and some even smaller ones
##### this is small
##### very small

also code highlighting

{% highlight python %}
import world, hacks
hacks.hack(world())
{% endhighlight %}

---

all this is made with Jekyll

but if you actually want to see the code for this new site

[you can go here](https://bitbucket.org/Syntox32/blog)
