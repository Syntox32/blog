---
layout: post
title:  "Write-up: LED Music Visualizer on a RaspberryPi"
date:   2017-05-04 12:00:00 +0000
excerpt_separator: <!--more-->
tags: [raspberry-pi, python, music visualization, networking, led-strip]
---

This was a rather time consuming project I did in April of 2015. This was a project
where the goal was to visualize music on an LED-strip.
<!--more-->
I used the RaspberryPi to be
able to communicate with the strip. The projects consists of FFT, SPI communication,
and some really wierd setup of client-server with caching of songs. Cobbled together with Python.


If you wanna see it in action here's a clip:

{% raw %}
  <video width="480" controls="controls">
    <source src="https://files.milje.me/LightsLightsBaby.webm" type="video/webm">
  </video>
{% endraw %}

*The song is 'C2C - Down the Road', for the curious.*

---

From what I remember this was on of my first advanced projects, and probably one of my favourites.
I learned so much, and I was really proud at the time when I got it working... but it's probably the most un-userfriendly thing I have made.

It was also my first try at doing anything with electronics, even just connecting three cables. Connecting LED-strip to my RaspberryPi without any idea of what I was doing, and finding out how to get them to light up was over half the fun.

#### **Getting it to play**

To be able to actually see a song on the strip, you have to do a couple of things:

* First of, SSH into the Raspberry Pi
* Make sure you have the server code on there
* Launch the server script with your desired settings.
* Convert the song on your other PC from MP3 to WAV, using the bundled batch script.
* Launch the client script with your desired settings.

*Your part in this is now over, the scripts do the rest from this point.*

* Let it run for some time while the FFT is all cached in a binary file.
* This binary "cache" file is now sent over to the server using TCP.
* The server then sends a message from the server to the client, telling it to start playing the song on your PC.
* At last the server/raspberrypi begins to light up the LED-strip with the values from the FFT.

---

*But did I mention you have to restart both scripts when you want a new song, or another frequenzy channel to show on the LED-strip?*

...so it's not exactly elegant, and that's probably why I like it so much. It was just a whole lot of fun putting together.

---

#### **Dive into the code**
[If you want to see the code you can go check out this thing on GitHub](https://github.com/Syntox32/Audiolyser).
