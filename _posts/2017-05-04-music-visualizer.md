---
layout: post
title:  "Write-up: Music Visualizer with kind-of Beat Detection"
date:   2017-05-04 12:00:00 +0000
excerpt_separator: <!--more-->
tags: [visualizer, c++, sfml, music, beat-detection]
---

This was a project I did in March of 2016. The project goal was to be able to visually visualize music, and to give a shot at implementing some beat detection.
<!--more-->
It works by taking Windows-loopback audio, feeding it through FFT, then rendering it with SFML, all written in C++.
The beat detection is sort of working, but requires a lot more work.

Here's a video of it in action:
{% raw %}
  <iframe width="560" height="315" src="https://www.youtube.com/embed/6osg3BHfjpA" frameborder="0" allowfullscreen></iframe>
{% endraw %}

*The song is 'IZECOLD - Swiggity, for those wondering.*

[DiveLight](https://github.com/Syntox32/DiveLight) became the successor to this project later that year.

---

#### **Dive into the code**
[If you want to see the code you can go check out this thing on GitHub](https://github.com/Syntox32/Visualizer).
