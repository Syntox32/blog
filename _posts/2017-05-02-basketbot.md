---
layout: post
title:  "Write-up: Messenger Basketball Bot"
date:   2017-05-02 12:00:00 +0000
excerpt_separator: <!--more-->
tags: [misc, c#, emgucv, sort-of-ai, image-recognition]
---

This is a little project I had in May of 2016. The point was to create a bot
to play the Messenger-app basketball game.
<!--more-->
The bot was made in C# and used image
recognition to find the ball and ADB to emulate input.

Here's a little webm of it doing its thing:

{% raw %}
  <video width="480" controls="controls">
    <source src="https://files.milje.me/2016-05-basketbot.webm" type="video/webm">
  </video>
{% endraw %}

*It gets to 16 points and then glitched out for some unknown reason.*

---

The whole bot is also made with a couple of third party components:

* **EmguCV**: OpenCV bindings for image recognition, to find the ball and the basket.
* **EmguCV OCR**: Text recognition, for keeping track of the number of points in the game.
* **ADB** : To emulate swipe input to the android phone.
* **Lego**: To build the stand for the 720p webcam, you can see it slightly in the webm above.

---

#### **Dive into the code**
[If you want to see the code you can go check out this thing on BitBuckettt](https://bitbucket.org/Syntox32/basket).

*The code was meant to be a one-off, so it's not exactly written for sustainability and maintainability.*
